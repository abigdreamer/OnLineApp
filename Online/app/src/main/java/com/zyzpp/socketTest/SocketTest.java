package com.zyzpp.socketTest;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.baidu.location.BDLocation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.Poi;
import com.example.administrator.OnLine.MainActivity;
import com.example.administrator.OnLine.MyApplication;
import com.zyzpp.bean.SocketBean;

public class SocketTest {
    private BufferedReader Reader = null;
    private BufferedWriter Writer = null;
    private Socket socket = null;
    private String message;
    private double latitude;
    private double longitude;
    private float radius;
    private String userId;
    private String otherId;
    private String addr,locationDescribe;
    private String ip="127.0.0.1";
    private String duankou="9898";
    Handler handle;

    public LocationClient mLocationClient = null;
    private LocationClientOption option=null;
    private MyLocationListener myListener= new MyLocationListener();
    private String top="获取位置未知";
    /**
     * 初始化Socket
     */
    public SocketTest(Handler handler) {
        this.handle=handler;
        queryuser();
        initSocket(ip,duankou);
        BaiDuMapStar(100*1000);
    }
    public SocketTest(){
        queryuser();
        initSocket(ip,duankou);
        BaiDuMapStar(100*1000);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOtherId() {
        return otherId;
    }

    public void setOtherId(String otherId) {
        this.otherId = otherId;
    }
    /**
     * 找值
     */
    private void queryuser(){
        SharedPreferences sharedPreferences=MyApplication.getGlobalContext().getSharedPreferences("user",
                Activity.MODE_PRIVATE);
        setUserId(sharedPreferences.getString("userid",""));
        setOtherId(sharedPreferences.getString("otherid",""));
        if(getUserId()==null||"".equals(getUserId())||getOtherId()==null|"".equals(getOtherId())){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("online",0);
            editor.commit();
        }
    }
    /**
     * 上传本地位置
     * @param location
     */
    public void showCurrentPosition(BDLocation location) {
        latitude = location.getLatitude();    //获取纬度信息
        longitude = location.getLongitude();    //获取经度信息
        radius = location.getRadius();    //获取定位精度，默认值为0.0f
        addr = location.getAddrStr();
        locationDescribe = location.getLocationDescribe();
        SocketBean upModel=new SocketBean(getUserId(),getOtherId(),latitude,longitude,radius,addr,locationDescribe);
        if (location.getLocType() == BDLocation.TypeGpsLocation){// GPS定位结果
            String speed="[速度:]"+location.getSpeed()+"千米/小时";// 单位：公里每小时
            String height="[高度:]"+location.getAltitude()+"米";// 单位：米
            upModel.setHeight(height);
            upModel.setSpeed(speed);
        }else{
            upModel.setSpeed("[速度:]对方未开启GPS");
            upModel.setHeight("[高度:]对方未开启GPS");
        }
        message = JSON.toJSONString(upModel);
        new Thread(new Runnable() {
            @Override
            public void run() {
                    try {
                        Writer.write(message + "\n");
                        Writer.flush();
                        Log.e("日志", "writer："+message);
                    }catch(Exception e) {
                        initSocket(ip,duankou);
                        Log.e("日志", "writer异常，重启socket" );
                    }
            }
        }).start();
    }
    /**
     * 初始化socket
     */
    private void initSocket(final String ipAdress, final String duankou) {
        //新建一个线程，用于初始化socket和检测是否有接收到新的消息
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String ip = ipAdress;//IP
                int port = Integer.parseInt(duankou);//SocketTest
                try {
                    socket = new Socket(ip, port);
                    Reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
                    Writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
                    String data;
                    while ((data = Reader.readLine()) != null) {
                        /*读取一行字符串，读取的内容来自于客户机 reader.readLine()方法是一个阻塞方法，*/
                        Message msg = new Message();
                        try{
                            SocketBean Bean =JSON.parseObject(data,SocketBean.class);
                            msg.obj = Bean;
                            msg.what = 1;
                            handle.sendMessage(msg);
                        }catch(Exception e){
                            msg.obj = data;
                            msg.what = 0;
                            handle.sendMessage(msg);
                        }
                    }
                    Writer.close();
                    Reader.close();
                    socket.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
    /**
    运行监控业务
    */
    public void BaiDuMapStar(int time){
        mLocationClient = new LocationClient(MyApplication.getGlobalContext());
        //注册监听函数
        mLocationClient.registerLocationListener(myListener);

        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
        //可选，设置定位模式，默认高精度
        //LocationMode.Hight_Accuracy：高精度；
        //LocationMode. Battery_Saving：低功耗；
        //LocationMode. Device_Sensors：仅使用设备；
        option.setCoorType("bd09ll");
        //可选，设置返回经纬度坐标类型，默认gcj02
        //gcj02：国测局坐标；
        //bd09ll：百度经纬度坐标；
        //bd09：百度墨卡托坐标；
        //海外地区定位，无需设置坐标类型，统一返回wgs84类型坐标
        option.setScanSpan(time);
        //可选，设置发起定位请求的间隔，int类型，单位ms
        //如果设置为0，则代表单次定位，即仅定位一次，默认为0
        //如果设置非0，需设置1000ms以上才有效
        option.setOpenGps(true);
        //可选，设置是否使用gps，默认false
        //使用高精度和仅用设备两种定位模式的，参数必须设置为true
        option.setIsNeedLocationPoiList(true);
        //可选，是否需要周边POI信息，默认为不需要，即参数为false
        //如果开发者需要获得周边POI信息，此处必须为true
        option.setIsNeedAddress(true);
        //可选，是否需要地址信息，默认为不需要，即参数为false
        //如果开发者需要获得当前点的地址信息，此处必须为true
        option.setLocationNotify(true);
        //可选，设置是否当GPS有效时按照1S/1次频率输出GPS结果，默认false
        option.setIgnoreKillProcess(true);
        //可选，定位SDK内部是一个service，并放到了独立进程。
        //设置是否在stop的时候杀死这个进程，默认（建议）不杀死，即setIgnoreKillProcess(true)
        option.setIsNeedLocationDescribe(true);
        //可选，是否需要位置描述信息，默认为不需要，即参数为false
        //如果开发者需要获得当前点的位置信息，此处必须为true
        option.SetIgnoreCacheException(false);
        //可选，设置是否收集Crash信息，默认收集，即参数为false
        option.setEnableSimulateGps(false);
        //可选，设置是否需要过滤GPS仿真结果，默认需要，即参数为false
        mLocationClient.setLocOption(option);
        //mLocationClient为第二步初始化过的LocationClient对象
        //需将配置好的LocationClientOption对象，通过setLocOption方法传递给LocationClient对象使用
        mLocationClient.start();
    }


    /**
     * 设置获取地址频率
     * @param time
     */
    public void setOptionTime(int time) {
        BaiDuMapStar(time);
    }

    class MyLocationListener implements BDLocationListener {
        // 异步返回的定位结果
        @Override
        public void onReceiveLocation(BDLocation location) {
            //此处的BDLocation为定位结果信息类，通过它的各种get方法可获取定位相关的全部结果
            //以下只列举部分获取经纬度相关（常用）的结果信息
            //更多结果信息获取说明，请参照类参考中BDLocation类中的说明

            double latitude = location.getLatitude();    //获取纬度信息
            double longitude = location.getLongitude();    //获取经度信息
            float radius = location.getRadius();    //获取定位精度，默认值为0.0f
            String coorType = location.getCoorType();
            //获取经纬度坐标类型，以LocationClientOption中设置过的坐标类型为准
            int errorCode = location.getLocType();
            //获取定位类型、定位错误返回码，具体信息可参照类参考中BDLocation类中的说明
            String addr = location.getAddrStr();    //获取详细地址信息
            String country = location.getCountry();    //获取国家
            String province = location.getProvince();    //获取省份
            String city = location.getCity();    //获取城市
            String district = location.getDistrict();    //获取区县
            String street = location.getStreet();    //获取街道信息
            String locationDescribe = location.getLocationDescribe();    //获取位置描述信息

            List<Poi> poiList = location.getPoiList();
            //获取周边POI信息
            //POI信息包括POI ID、名称等，具体信息请参照类参考中POI类的相关说明
            if (location.getLocType() == BDLocation.TypeGpsLocation){// GPS定位结果
//                String speed="[速度:]"+location.getSpeed()+"千米/小时";// 单位：公里每小时
//                String height="[高度:]"+location.getAltitude()+"米";// 单位：米
                top="GPS定位成功";
            } else if (location.getLocType() == BDLocation.TypeNetWorkLocation){// 网络定位结果
                top="网络定位成功";
            } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                top="离线定位成功，离线定位结果也是有效的";
            } else if (location.getLocType() == BDLocation.TypeServerError) {
                top="服务端网络定位失败，可以反馈IMEI号和大体定位时间到loc-bugs@baidu.com，会有人追查原因";
            } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                top="网络不同导致定位失败，请检查网络是否通畅";
            } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                top="无法获取有效定位依据导致定位失败，一般是由于手机未给予定位权限的原因";
            }
            if (MainActivity.count==1) {
                Toast.makeText(MyApplication.getGlobalContext(), top, Toast.LENGTH_LONG).show();
                MainActivity.count=2;
            }
            //上传本客户端地理位置
            showCurrentPosition(location);
        }
    }
}
